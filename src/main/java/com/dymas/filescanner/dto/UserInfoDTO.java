package com.dymas.filescanner.dto;

import lombok.*;

import javax.validation.constraints.NotEmpty;

/**
 * Projet :  file-scanner
 * Package :  com.dymas.filescanner.dto
 * File :  UserInfoDTO
 * Created on : 2022, Thursday 17 of February
 * Created at : 10:54 AM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserInfoDTO {
    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;
}
