package com.dymas.filescanner.service;


import com.cloudmersive.client.ScanApi;
import com.cloudmersive.client.invoker.ApiClient;
import com.cloudmersive.client.invoker.ApiException;
import com.cloudmersive.client.invoker.Configuration;
import com.cloudmersive.client.invoker.auth.ApiKeyAuth;
import com.cloudmersive.client.model.VirusScanResult;
import com.dymas.filescanner.domain.StoredFile;
import com.dymas.filescanner.dto.UserInfoDTO;
import com.dymas.filescanner.exceptions.FileInfectedException;
import com.dymas.filescanner.exceptions.ItemNotFoundException;
import com.dymas.filescanner.repository.StoredFileRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;


/**
 * Projet :  file-scanner
 * Package :  com.dymas.filescanner
 * File :  StoredFileService
 * Created on : 2022, Thursday 17 of February
 * Created at : 9:12 AM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
@Service
@RequiredArgsConstructor
public class StoredFileService {

    private final StoredFileRepository repository;

    @Value("${upload.path}")
    private String uploadPath;

    @PostConstruct
    public void init() {
        try {
            Files.createDirectories(Paths.get(getUploadPath()));
        } catch (IOException e) {
            throw new RuntimeException("Could not create upload folder!");
        }
    }

    private String getUploadPath() {
        return System.getProperty("user.dir") + uploadPath;
    }

    @Transactional
    public StoredFile save(UserInfoDTO userInfo, MultipartFile file) throws IOException {
        if (!fileIsSafe(file))
            throw new FileInfectedException(file.getOriginalFilename());

        Path root = Paths.get(getUploadPath());
        if (!Files.exists(root))
            init();
        final StoredFile data = StoredFile.builder()
            .firstName(userInfo.getFirstName())
            .lastName(userInfo.getLastName())
            .size(file.getSize())
            .url(getUploadPath() + "/" + file.getOriginalFilename())
            .fileName(file.getOriginalFilename())
            .build();
        final StoredFile save = repository.save(data);
        Files.copy(file.getInputStream(), root.resolve(Objects.requireNonNull(file.getOriginalFilename())));
        return save;
    }

    public List<StoredFile> findAll() {
        return repository.findAll();
    }


    public Resource load(String filename) {
        try {
            Path file = Paths.get(getUploadPath())
                .resolve(filename);
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

    public Resource downloadFile(Long id) throws Exception {
        StoredFile storedFile = repository.findById(id).orElseThrow(() -> new ItemNotFoundException(id));
        Path file = Paths.get(getUploadPath()).resolve(storedFile.getFileName());
        repository.deleteById(id);
        return new UrlResource(file.toUri());
    }

    @Transactional
    public void deleteFile(Long id) throws IOException {
        StoredFile storedFile = repository.findById(id).orElseThrow(() -> new ItemNotFoundException(id));
        Files.deleteIfExists(Paths.get(storedFile.getUrl()));
        repository.deleteById(id);
    }

    private boolean fileIsSafe(MultipartFile file) throws IOException {
        final File fileToScan = getTempFileFromMultiPart(file);

        ApiClient defaultClient = Configuration.getDefaultApiClient();

        // Configure API key authorization: Apikey
        ApiKeyAuth Apikey = (ApiKeyAuth) defaultClient.getAuthentication("Apikey");
        Apikey.setApiKey("9cb4b004-ad5b-44cb-a971-ba58b4f9dc0a");

        ScanApi apiInstance = new ScanApi();
        try {
            VirusScanResult result = apiInstance.scanFile(fileToScan);
            return result.isCleanResult();
        } catch (ApiException e) {
            System.err.println("Exception when calling ScanApi#scanFile");
            e.printStackTrace();
        }
        return false;
    }

    private File getTempFileFromMultiPart(MultipartFile file) throws IOException {
        File tmpFile = File.createTempFile("tmpFileScan", "", null);
        try (OutputStream os = new FileOutputStream(tmpFile)) {
            os.write(file.getBytes());
        }
        return tmpFile;
    }
}
