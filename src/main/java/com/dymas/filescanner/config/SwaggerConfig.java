package com.dymas.filescanner.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Projet :  file-scanner-web
 * Package :  com.dymas.filescanner.config
 * File :  SwaggerConfig
 * Created on : 2022, Friday 18 of February
 * Created at : 8:35 AM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
@Configuration
public class SwaggerConfig {
    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI().info(new Info()
            .title("FileScanner API Documentation")
            .version("V1.0")
            .description("This is the FileScanner documentation created using springdocs - " +
                "a library for OpenAPI 3 with spring boot.")
            .termsOfService("http://swagger.io/terms/")
            .license(new License().name("Apache 2.0")
                .name("Brice GUEMKAM")
                .url("http://springdoc.org")));
    }
}
