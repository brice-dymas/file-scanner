package com.dymas.filescanner.config;

import lombok.*;

/**
 * Projet :  file-scanner
 * Package :  com.dymas.filescanner.config
 * File :  ApiResponse
 * Created on : 2022, Thursday 17 of February
 * Created at : 12:25 PM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiResponse {

    @Builder.Default
    private boolean success = true;

    @Builder.Default
    private String message = "Operation successfully completed!";

    private Object data;
}
