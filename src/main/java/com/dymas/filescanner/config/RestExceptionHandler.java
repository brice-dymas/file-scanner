package com.dymas.filescanner.config;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.IOException;

/**
 * Projet :  file-scanner
 * Package :  com.dymas.filescanner.config
 * File :  RestExceptionHandler
 * Created on : 2022, Thursday 17 of February
 * Created at : 9:23 AM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiResponse> handleCasualExceptions(Exception ex) {

        ApiResponse failure = ApiResponse.builder().success(false).message("Unable to complete this operation!").data(ex.getMessage()).build();

        if (ex instanceof MaxUploadSizeExceededException)
            failure = ApiResponse.builder().message("Unable to upload. File is too large!").data(ex.getMessage()).build();

        if (ex instanceof IOException || ex instanceof NullPointerException)
            failure = ApiResponse.builder().message("Unable to open the resource you are looking for!").data(ex.getMessage()).build();

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(failure);
    }
}
