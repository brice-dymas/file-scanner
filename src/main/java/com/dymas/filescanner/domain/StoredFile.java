package com.dymas.filescanner.domain;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDateTime;

/**
 * Projet :  file-scanner
 * Package :  com.dymas.filescanner.domain
 * File :  StoredFile
 * Created on : 2022, Thursday 17 of February
 * Created at : 9:18 AM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StoredFile {
    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;
    @NotEmpty
    private String fileName;
    @NotEmpty
    private String url;

    @NotNull
    @Builder.Default
    LocalDateTime createdOn = LocalDateTime.now();

    @NotNull
    @Positive
    private Long size;
}
