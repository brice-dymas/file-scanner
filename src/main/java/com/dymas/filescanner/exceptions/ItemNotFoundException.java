package com.dymas.filescanner.exceptions;

import static java.lang.String.format;

/**
 * Projet :  file-scanner
 * Package :  com.dymas.filescanner.exceptions
 * File :  ItemNotFoundException
 * Created on : 2022, Thursday 17 of February
 * Created at : 12:48 PM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
public class ItemNotFoundException extends RuntimeException {
    public ItemNotFoundException(Object valueNotFound) {
        super(format("The file identified with the value %s doesn't exists!", valueNotFound));
    }
}
