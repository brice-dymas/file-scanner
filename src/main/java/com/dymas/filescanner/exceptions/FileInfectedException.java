package com.dymas.filescanner.exceptions;

import static java.lang.String.format;

/**
 * Projet :  file-scanner
 * Package :  com.dymas.filescanner.exceptions
 * File :  FileInfectedException
 * Created on : 2022, Thursday 17 of February
 * Created at : 3:31 PM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
public class FileInfectedException extends RuntimeException {
    public FileInfectedException(String filename) {
        super(format("The file %s you tried to upload is corrupted by viruses", filename));
    }
}
