package com.dymas.filescanner.controller;

import com.dymas.filescanner.config.ApiResponse;
import com.dymas.filescanner.domain.StoredFile;
import com.dymas.filescanner.dto.UserInfoDTO;
import com.dymas.filescanner.service.StoredFileService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Projet :  file-scanner
 * Package :  com.dymas.filescanner.controller
 * File :  StoredFilesController
 * Created on : 2022, Thursday 17 of February
 * Created at : 9:20 AM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
@CrossOrigin
@RestController
@RequestMapping("files")
@RequiredArgsConstructor
public class StoredFilesController {

    private final StoredFileService fileService;

    @Operation(description = "This methods allows you to upload a file with user details")
    @PostMapping("/upload")
    public ResponseEntity<ApiResponse> uploadFile(@Valid @RequestBody UserInfoDTO userInfo, MultipartFile file) throws Exception {
        final StoredFile data = fileService.save(userInfo, file);
        return ResponseEntity.status(HttpStatus.CREATED).body(ApiResponse.builder().data(data).build());
    }

    @Operation(description = "List all the stored files")
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> getAllFiles() {
        final List<StoredFile> data = fileService.findAll();
        return ResponseEntity.ok(ApiResponse.builder().data(data).build());
    }

    @Operation(description = "Uploads a file with user details given as  requests params")
    @PostMapping("/upload/bad-practice")
    public ResponseEntity<ApiResponse> uploadFilerp(@RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName, @RequestParam("file") MultipartFile file) throws Exception {
        final StoredFile data = fileService.save(UserInfoDTO.builder().firstName(firstName).lastName(lastName).build(), file);
        return ResponseEntity.status(HttpStatus.OK).body(ApiResponse.builder().data(data).build());
    }

    @Operation(description = "Delete an uploaded file both in DB and disk storage")
    @DeleteMapping("/{id}/delete")
    public ResponseEntity<ApiResponse> delete(@NotNull @PathVariable Long id) throws Exception {
        fileService.deleteFile(id);
        return ResponseEntity.status(HttpStatus.OK).body(ApiResponse.builder().build());
    }

    @Operation(description = "Download the content of an uploaded file using a given ID")
    @Parameter(name = "id", required = true, example = "1")
    @GetMapping("/{id}/download")
    @ResponseBody
    public ResponseEntity<Resource> downloadFile(@NotNull @PathVariable Long id) throws Exception {
        System.out.println("downloading");
        Resource file = fileService.downloadFile(id);
        return ResponseEntity.ok()
            .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
            .body(file);
    }
}
