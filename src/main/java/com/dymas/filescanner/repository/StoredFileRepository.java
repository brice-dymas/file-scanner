package com.dymas.filescanner.repository;

import com.dymas.filescanner.domain.StoredFile;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.validation.constraints.NotEmpty;

/**
 * Projet :  file-scanner
 * Package :  com.dymas.filescanner.repository
 * File :  StoredFileRepository
 * Created on : 2022, Thursday 17 of February
 * Created at : 10:19 AM
 * Author name : Brice dymas
 * Author's mail : briceguemkam@gmail.com / brice.guemkam@iforce5.com
 */
public interface StoredFileRepository extends JpaRepository<StoredFile, Long> {

    boolean existsByFileName(@NotEmpty String fileName);
}
