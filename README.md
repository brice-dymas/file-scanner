# Getting Started with FileScanner App

### Guides

In order to run this application, You need to install at least the Java Development Kit v11. There's an integrated maven
wrapper in the project so no need to install a Maven tool on your system. The DB type in here is H2

* To run the app open a terminal at the root's project folder and type the following command `./mvnw spring-boot:run`
* The Application starts now and is available on the 8080 port
* You can access the Swagger documentation at [http://localhost:8080/refdocs](http://localhost:8080/refdocs)

